import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  title='About us page title';

  constructor(private meta: Meta, private titleService: Title,) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
  }

}
