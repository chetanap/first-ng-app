import { Component, OnInit } from '@angular/core';
@Component({
  //selector: '[app-servers]', // selectors can also be made using attributes
  selector: 'app-basics',
  templateUrl: './basics.component.html', 
  //template: `<app-servers></app-servers>`, // template is the alternate to templateUrl. Templates are important
  styleUrls: ['./basics.component.css']
})
export class BasicsComponent implements OnInit {
	allowNewServer = false;
	onServerCreationStatus = "No server was created!"
	onServerCreationStatus2 = "Server is yet to be created!";
  serverName = 'Test Server ';
  anotherServer = 'Another Test Server ';
  anotherServerWithElse = 'Another Server with Else condition';
  userName = '';
  addServerWithDirective = false;
  addServerWithElseCondition = false;
  onServerCreationWithElse = 'Server created with Else Condition';
  constructor() {
  	setTimeout (() => {
  		this.allowNewServer = true;
  	}, 2000)
  }
  ngOnInit() {
  }
  onCreateServer(){
  	this.onServerCreationStatus = "The server was created with Click Event."
  }
  onUpdateServerName(event:Event){
    //console.log(event)
  	this.serverName = (<HTMLInputElement>event.target).value;
  }

  onCreateServer2(){
    this.addServerWithDirective= true;
  	this.onServerCreationStatus2 = "The server was created! Name is " + this.anotherServer;
  }
  onCreateServer3(){
    this.addServerWithElseCondition = true;
  }
  getUserName(){
    this.userName = (<HTMLInputElement>event.target).value;
  }
  // addWithDirectives(){
  //   this.addServerWithDirective=true;
  // }
}
